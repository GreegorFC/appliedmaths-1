﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace AppliedMathsAssessment
{
    class PopUp : Sprite
    {
        // ------------------
        // Data
        // ------------------
        Text popUpText;
        Button popUpButton;
        int buttonWidth;

        bool showing = false;
        float showTimePassed = 0;
        const float ANIMATION_TIME = 1f;
        Vector2 OffscreenPos;
        Vector2 OnscreenPos;


        // ------------------
        // Behaviour
        // ------------------
        public PopUp(
            Texture2D newTexture,
            Texture2D newButtonTexture,
            Texture2D newPressedTexture,
            SpriteFont newButtonFont,
            SoundEffect newClickSFX,
            SpriteFont newPanelFont,
            string panelText,
            string buttonText,
            GraphicsDevice graphics)
            : base(newTexture)
        {
            popUpText = new Text(newPanelFont);
            popUpText.SetTextString(panelText);
            popUpText.SetAlignment(Text.TextAlignment.CENTRE);
            popUpText.SetColor(Color.Black);
            popUpButton = new Button(newButtonTexture, newPressedTexture, newButtonFont, newClickSFX);
            popUpButton.SetString(buttonText);
            buttonWidth = newButtonTexture.Width;

            OnscreenPos = new Vector2(
                graphics.Viewport.Bounds.Width / 2 - newTexture.Width / 2,
                graphics.Viewport.Bounds.Height / 2 - newTexture.Height / 2);
            OffscreenPos = OnscreenPos;
            OffscreenPos.Y = 1000;

            SetPosition(OffscreenPos);
        }
        // ------------------
        public override void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
            popUpText.SetPosition(position + new Vector2(texture.Width / 2, +50f));
            popUpButton.SetPosition(position + new Vector2(texture.Width / 2 - buttonWidth / 2, +100f));
        }
        // ------------------
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            popUpText.Draw(spriteBatch);
            popUpButton.Draw(spriteBatch);
        }
        // ------------------
        public bool IsClicked()
        {
            return popUpButton.IsClicked();
        }
        // ------------------
        public void Show()
        {
            if (!showing)
            {
                showing = true;
                showTimePassed = 0;
            }
        }
        // ------------------
        public void Hide()
        {
            if (showing)
            {
                showing = false;
                SetPosition(OffscreenPos);
            }
        }
        // ------------------
        public void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (showing)
            {
                // update timer
                showTimePassed += dt;

                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 4 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////

                // OLD VERSION, DEPRECATED
                // Re-write this to move the pop-up using a cubic interpolation
                //SetPosition(OnscreenPos);

                if(showTimePassed < ANIMATION_TIME)
                {
                    // determine settings
                    float duration = ANIMATION_TIME;
                    float time = showTimePassed;
                    Vector2 begin = OffscreenPos;
                    Vector2 change = OnscreenPos - OffscreenPos;


                    //determine constansts
                    Vector2 k1 = -2* change  / (duration * duration * duration);
                    Vector2 k2 = 3 * change/(duration * duration);
                    Vector2 k3 = Vector2.Zero;
                    Vector2 k4 = begin;

                    //plug into equation
                    Vector2 newPos = k1 * time * time * time + k2 * time * time + k3 * time + k4 ;
                    SetPosition(newPos);

                    
                        
                }

               /* // If the player has clicked,
                // Start the transition

                // Set all of our interpolation data


                // Begin is the starting position for the transition
               Vector2 begin = OffscreenPos;

                // End is our target position to end at (OnscreenPos)
                Vector2 end = new Vector2(OnscreenPos.X, OnscreenPos.Y);

                // THe direction and distance we will move by the end of the transition
                Vector2 change = end - begin;

                // How long the transition will take in seconds
                float duration = ANIMATION_TIME;

                // How long it has been since the start of the transition
                float time = showTimePassed;


                // Add to the time since we start our transition
                time += (float)gameTime.ElapsedGameTime.TotalSeconds;

                // Check if our transition is over
                if (time >= duration)
                {
                    // the transition has ended
                    // Set us to be at our target position
                    position = begin + change;
                    SetPosition(OnscreenPos);
                }
                else
                {
                    // transition is still going

                    

                   

                    // Quad Ease Out
                    Vector2 k1 = -change / (duration * duration);
                    Vector2 k2 = 2 * change / duration;
                    Vector2 k3 = begin;
                    SetPosition (k1 * time * time + k2 * time + k3);

                    //Cubic ease out
                    //Vector2 k1 = -change / (duration * duration * duration);
                    //Vector2 k2 = -change / (duration * duration);
                    //Vector2 k3 = 2 * change / duration;
                    //Vector2 k4 = begin;
                    //SetPosition(k1 * time * time + k2 * time + k3);

                }*/

                ///////////////////////////////////////////////////////////////////  
                // END TASK 4 CODE
                ///////////////////////////////////////////////////////////////////  
            }

        }
        // ------------------
    }
}
